<?php
/**
 * Created by PhpStorm.
 * User: msegura
 * Date: 2019-05-24
 * Time: 23:01
 */

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::THEME,
    'frontend/Test/theme',
    __DIR__
);
